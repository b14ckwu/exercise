package com.five.exercise;

import com.five.exercise.util.Tools;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

@SpringBootTest
class ExerciseApplicationTests {

    @Test
    void contextLoads() {
        String imgUrl = "D:\\\\AIExercise\\\\IDCard1_meitu_2.jpg";
        String side="back";
        HashMap<String, String> map = Tools.getIDCardInfo(imgUrl,side);
//        Collection<String> values = map.values();
//        Iterator<String> iterator2 = values.iterator();
//        while (iterator2.hasNext()) {
//            System.out.print(iterator2.next() + ", ");
//        }
        System.out.println(map);
    }

    @Test
    void testGeneralBasic(){
        String imgUrl = "D:\\\\AIExercise\\\\IDCard1_meitu_2.jpg";
        HashMap<String, String> map = Tools.general_basic(imgUrl);
    }

    @Test
    void testLicensePlate(){
        String imgUrl = "D:\\\\AIExercise\\\\u=3938533524,1309406965&fm=26&gp=0.jpg";
        HashMap<String, String> map = Tools.license_plate(imgUrl);
    }

    @Test
    void testQuotaInvoice(){
        String imgUrl = "D:\\\\AIExercise\\\\timg (1).jpg";
        HashMap<String, String> map = Tools.quota_invoice(imgUrl);
    }
}
