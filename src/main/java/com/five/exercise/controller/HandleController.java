package com.five.exercise.controller;

import com.five.exercise.dto.Result;
import com.five.exercise.util.Tools;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

@Controller
public class HandleController {
    @Value("${web.upload-path}")
    private String filePath;  //获取上传地址

    @RequestMapping("/upload")
    @ResponseBody
    public Result upload(MultipartFile upload,String side,String type) {
        // 判断，该路径是否存在
        File file = new File(filePath);
        if (!file.exists()) {
            // 创建该文件夹
            file.mkdirs();
        }
        // 说明上传文件项
        // 获取上传文件的名称
        String filename = upload.getOriginalFilename();
        // 把文件的名称设置唯一值，uuid
        String uuid = UUID.randomUUID().toString().replace("-", "");
        filename = uuid + "_" + filename;
        String imgUrl=filename; //配置虚拟路径，让浏览器能访问静态资源
        try {
            // 完成文件上传
            upload.transferTo(new File(filePath, filename));
        } catch (Exception e) {
            return Result.fail("文件上传失败！");
        }
        filename=filePath+"\\"+filename;
        System.out.println(filename);
        HashMap<String, String> map=null;
        if("idcard".equals(type)){
            map = Tools.getIDCardInfo(filename,side);
            map.put("side",side);
        }else if("general_basic".equals(type)){
            map = Tools.general_basic(filename);
        }else if("license_plate".equals(type)){
            map = Tools.license_plate(filename);
        }else if("quota_invoice".equals(type)){
            map = Tools.quota_invoice(filename);
        }
        map.put("imgUrl",imgUrl);
        map.put("type",type);
        return Result.success(map);
    }
}
