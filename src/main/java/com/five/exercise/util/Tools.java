package com.five.exercise.util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class Tools {
    public static String request(String httpUrl, String httpArg) {
        BufferedReader reader = null;
        String result = null;
        StringBuffer sbf = new StringBuffer();
        try {
//用java JDK自带的URL去请求
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
//设置该请求的消息头
//设置HTTP方法：POST
            connection.setRequestMethod("POST");
//设置其Header的Content-Type参数为application/x-www-form-urlencoded
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
// 填入apikey到HTTP header
            connection.setRequestProperty("apikey", "uml8HFzu2hFd8iEG2LkQGMxm");
//将第二步获取到的token填入到HTTP header
            connection.setRequestProperty("access_token", baiduOcr.getAuth());
            connection.setDoOutput(true);
            connection.getOutputStream().write(httpArg.getBytes("UTF-8"));
            connection.connect();
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            result = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //解析身份证正面的json字符串
    public static HashMap<String,String> getHashMapByJsonFront(String jsonResult){
        HashMap map = new HashMap<String,String>();
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            JSONObject words_result= jsonObject.getJSONObject("words_result");
            Iterator<String> it = words_result.keys();
            while (it.hasNext()){
                String key = it.next();
                JSONObject result = words_result.getJSONObject(key);
                String value=result.getString("words");
                switch (key){
                    case "姓名":
                        map.put("name",value);
                        break;
                    case "民族":
                        map.put("nation",value);
                        break;
                    case "住址":
                        map.put("address",value);
                        break;
                    case "公民身份号码":
                        map.put("IDCard",value);
                        break;
                    case "出生":
                        map.put("Birth",dateStringAnaly(value));
                        break;
                    case "性别":
                        map.put("sex",value);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //解析身份证背面的json字符串
    public static HashMap<String,String> getHashMapByJsonBack(String jsonResult){
        HashMap map = new HashMap<String,String>();
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            JSONObject words_result= jsonObject.getJSONObject("words_result");
            Iterator<String> it = words_result.keys();
            while (it.hasNext()){
                String key = it.next();
                JSONObject result = words_result.getJSONObject(key);
                String value=result.getString("words");
                switch (key){
                    case "签发机关":
                        map.put("bureau",value);
                        break;
                    case "签发日期":
                        map.put("issueDate",dateStringAnaly(value));
                        break;
                    case "失效日期":
                        map.put("expirationDate",dateStringAnaly(value));
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //解析通用文字识别的json字符串
    public static HashMap<String,String> getHashMapByJsonGeneralBasic(String jsonResult){
        HashMap map = new HashMap<String,Object>();
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            JSONArray words_result= jsonObject.getJSONArray("words_result");
            List<String> collection=new ArrayList<String>();
            for(int i=0;i<words_result.length();i++){
                //3、把里面的对象转化为JSONObject
                JSONObject job = words_result.getJSONObject(i);
                // 4、把里面想要的参数一个个用.属性名的方式获取到
                String value=job.getString("words");
                collection.add(value);
            }
            map.put("collection",collection);
            System.out.println(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //解析车牌识别的json字符串
    public static HashMap<String,String> getHashMapByJsonLicensePlate(String jsonResult){
        HashMap map = new HashMap<String,String>();
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            JSONObject words_result= jsonObject.getJSONObject("words_result");
            map.put("number",words_result.getString("number"));
            System.out.println(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    //解析发票识别的json字符串
    public static HashMap<String,String> getHashMapByJsonQuotaInvoice(String jsonResult){
        HashMap map = new HashMap<String,String>();
        try {
            JSONObject jsonObject = new JSONObject(jsonResult);
            JSONObject words_result= jsonObject.getJSONObject("words_result");
            Iterator<String> it = words_result.keys();
            while (it.hasNext()){
                String key = it.next();
                String value=words_result.getString(key);
                switch (key){
                    case "invoice_number":
                        map.put("invoice_number",value);
                        break;
                    case "avg_score":
                        map.put("avg_score",value);
                        break;
                    case "invoice_rate":
                        map.put("invoice_rate",value);
                        break;
                    case "invoice_code":
                        map.put("invoice_code",value);
                        break;
                }
            }
            System.out.println(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static HashMap<String,String> getIDCardInfo(String imgUrl,String side){
        File file = new File(imgUrl);
//进行BASE64位编码
        String imageBase = BASE64.encodeImgageToBase64(file);
        imageBase = imageBase.replaceAll("\r\n", "");
        imageBase = imageBase.replaceAll("\\+", "%2B");
//百度云的文字识别接口,后面参数为获取到的token
        String httpUrl = "";
        httpUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token=" + baiduOcr.getAuth();
        String httpArg = "";
        if("front".equals(side)){
            httpArg = "detect_direction=true&id_card_side=front&image=" + imageBase;
        }else if("back".equals(side)){
            httpArg = "detect_direction=true&id_card_side=back&image=" + imageBase;
        }
        String jsonResult = request(httpUrl, httpArg);
        System.out.println("返回的结果--------->" + jsonResult);
        HashMap<String, String> map = null;
        if("front".equals(side)){
            map=getHashMapByJsonFront(jsonResult);
        }else if("back".equals(side)){
            map=getHashMapByJsonBack(jsonResult);
        }
        return map;
    }

    public static HashMap<String,String> general_basic(String imgUrl){
        File file = new File(imgUrl);
//进行BASE64位编码
        String imageBase = BASE64.encodeImgageToBase64(file);
        imageBase = imageBase.replaceAll("\r\n", "");
        imageBase = imageBase.replaceAll("\\+", "%2B");
//百度云的文字识别接口,后面参数为获取到的token
        String httpUrl = "";
        httpUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=" + baiduOcr.getAuth();
        String httpArg = "";
        httpArg = "detect_direction=true&image=" + imageBase;
        String jsonResult = request(httpUrl, httpArg);
        System.out.println("返回的结果--------->" + jsonResult);
        HashMap<String, String> map = null;
        map=getHashMapByJsonGeneralBasic(jsonResult);
        return map;
    }

    public static HashMap<String,String> license_plate(String imgUrl){
        File file = new File(imgUrl);
//进行BASE64位编码
        String imageBase = BASE64.encodeImgageToBase64(file);
        imageBase = imageBase.replaceAll("\r\n", "");
        imageBase = imageBase.replaceAll("\\+", "%2B");
//百度云的文字识别接口,后面参数为获取到的token
        String httpUrl = "";
        httpUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/license_plate?access_token=" + baiduOcr.getAuth();
        String httpArg = "";
        httpArg = "image=" + imageBase;
        String jsonResult = request(httpUrl, httpArg);
        System.out.println("返回的结果--------->" + jsonResult);
        HashMap<String, String> map = null;
        map=getHashMapByJsonLicensePlate(jsonResult);
        return map;
    }

    public static HashMap<String,String> quota_invoice(String imgUrl){
        File file = new File(imgUrl);
//进行BASE64位编码
        String imageBase = BASE64.encodeImgageToBase64(file);
        imageBase = imageBase.replaceAll("\r\n", "");
        imageBase = imageBase.replaceAll("\\+", "%2B");
//百度云的文字识别接口,后面参数为获取到的token
        String httpUrl = "";
        httpUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/quota_invoice?access_token=" + baiduOcr.getAuth();
        String httpArg = "";
        httpArg = "image=" + imageBase;
        String jsonResult = request(httpUrl, httpArg);
        System.out.println("返回的结果--------->" + jsonResult);
        HashMap<String, String> map = null;
        map=getHashMapByJsonQuotaInvoice(jsonResult);
        return map;
    }

    public static String dateStringAnaly(String date){
        String year=date.substring(0,4);
        String month=date.substring(4,6);
        String day=date.substring(6,8);
        String newStr=year+"年"+month+"月"+day+"日";
        return newStr;
    }
}
